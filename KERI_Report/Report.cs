﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Data.OleDb;
using System.IO;

namespace KERI_Report
{
    public partial class Report : Form
    {
        TagSearch ts;
        int i;
        int j;
        int y;
        public Report()
        {
            InitializeComponent();
        }
        public Report(TagSearch _Form)
        {
            InitializeComponent();
            ts = _Form;
        }

        private void Report_Load(object sender, EventArgs e)
        {
            comboBox1.Text = "1";
            comboBox1.Items.Add("1");
            comboBox1.Items.Add("10");

            comboBox2.Text = "M";
            comboBox2.Items.Add("S");
            comboBox2.Items.Add("M");
            comboBox2.Items.Add("H");
                       
            radioButton2.Checked = true;            
            listView3.Columns.Add("", 20);
            listView3.Columns.Add("TagName", 150);
            listView3.Columns.Add("Description", 150);            
        }
        public int recordCount;//레코드개수
        public int lvtCount; // 리스트뷰 아이템 개수
        public int itagNumber;
        public double[,] ListTag = new double[1, 1]; //데이터개수,태그명
        public DateTime[] ListTagTime = new DateTime[1];

        public void button1_Click(object sender, EventArgs e) //Data Search 클릭
        {
            OleDbConnection conn = new OleDbConnection("Provider=iHistorian OLE DB Provider;;User ID=;Password=;Data Source=;Mode=Read");   
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = conn;                
            conn.Open();                
            
            string sDate = dateTimePicker1.Value.ToString("yyyy-MM-dd");
            string eDate = dateTimePicker2.Value.ToString("yyyy-MM-dd");
            string tag;
            recordCount = 0;
            itagNumber = 0;
            j = 0;
            foreach (ListViewItem item in listView3.CheckedItems) //체크된 항목만 골라내기
            {
                i = 0;
                tag = item.SubItems[1].Text; // 태그명
                int dst = (int)numTimeBigin.Value; // 시작시간 
                int den = (int)numTimeEnd.Value; //   마지막시간 
                lvtCount = listView3.Items.Count; //태그개수
                
                cmd.CommandText = "SELECT value, timestamp FROM ihRawData Where tagname = '" + item.SubItems[1].Text + "' and timestamp BETWEEN '" + sDate + " " + dst + ":00:00' and '"
                + eDate + " " + den + ":59:59' and intervalmilliseconds = " + comboBox1.Text + comboBox2.Text + " and rowcount = 200000";
                OleDbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    recordCount = i ++ ;   
                }
                reader.Close();

                itagNumber = recordCount;// 데이터개수

                cmd.CommandText = "SELECT value, timestamp FROM ihRawData Where tagname = '" + item.SubItems[1].Text + "' and timestamp BETWEEN '" + sDate + " " + dst + ":00:00' and '"
                + eDate + " " + den + ":59:59' and intervalmilliseconds = " + comboBox1.Text + comboBox2.Text + " and rowcount = 200000";
                OleDbDataReader reader2 = cmd.ExecuteReader();
                i = 0;

                if (j == 0)
                {
                    ListTag = new double[itagNumber + 1, lvtCount];
                    ListTagTime = new DateTime[itagNumber + 1];
                }
                
                while (reader2.Read())
                {
                    ListTag[i, j] = (double)reader2[0];
                    ListTagTime[i] = Convert.ToDateTime(reader2[1]);
                    i = i+1 ;
                }
                reader2.Close();
                j = j + 1;
            }
            conn.Close();

            SaveFileDialog saveDlg = new SaveFileDialog();
            saveDlg.Filter = "CSV 파일(.CSV)|*.csv";

            if (saveDlg.ShowDialog() == DialogResult.OK)
            {
                SaveData(saveDlg.FileName);
                MessageBox.Show("저장 완료!");
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "10")
            {
                comboBox2.Items.Clear();
                comboBox2.Text = "M";
            }
            else
            {
                comboBox2.Text = "S";
                comboBox2.Items.Add("S");
                comboBox2.Items.Add("M");
                comboBox2.Items.Add("H");
            }
        }

        private void SaveData(string fileName)
        {
            i = 0;
            j = 0;
            string strPrint;
            string strLvtName;
            using (StreamWriter tWriter = new StreamWriter(fileName, true, Encoding.Default))
                
            for (i = 0; i <= itagNumber -1; i++)  //ex)119 데이터수
            {
                if (j == 0)
                {
                    tWriter.WriteLine("");
                    strPrint = "";
                    for (y = 0; y <= listView3.Items.Count-1; y++ )
                    {
                        strLvtName = listView3.Items[y].SubItems[1].Text;
                        strPrint += string.Format("{0},",strLvtName); //태그이름
                    }
                    tWriter.WriteLine(string.Format("{0},{1}", "", strPrint));
                }
                strPrint = "";
                strPrint += string.Format("{0},", ListTagTime[i]); //시간
                for (j = 0; j <= lvtCount-1; j++) //2 태그수
                {
                    strPrint += string.Format("{0},", ListTag[i, j]); //태그     
                }
                tWriter.WriteLine(string.Format(strPrint));
            }
        }
        
        private void button4_Click(object sender, EventArgs e)
        {
            TagSearch ts = new TagSearch(this);
            listView3.Items.Clear();
            ts.ShowDialog();
            listView3.BeginUpdate();
            for (i = 0; i <= ts.itemCount - 1; i++)
            {
                ListViewItem lvt = new ListViewItem("",i);
                lvt.SubItems.Add(ts.listView2.Items[i].SubItems[1].Text);
                lvt.SubItems.Add(ts.listView2.Items[i].SubItems[2].Text);
                listView3.Items.Add(lvt);
                lvt.Checked = true; 
                listView3.EndUpdate();
            }
        }

        public void listview3Add(int key)
        {            
            for (int i = 0; i < key; i++)
            {
                ListViewItem lvt = new ListViewItem("lvt", i);                
                lvt.Checked = true; 
                //lvt.SubItems.Add(ReportClass.bListTag[i]);
                //lvt.SubItems.Add(ReportClass.bListDescription[i]);
                listView3.Items.Add(lvt);
            }            
        }
        public string strPrint { get; set; }
    }
}

